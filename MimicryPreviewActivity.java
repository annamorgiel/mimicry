package de.emotisk.scott.playingfields.mimicry;

import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.emotisk.scott.R;

import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.ANGRY;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.HAPPY;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.SAD;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.SURPRISED;

public class MimicryPreviewActivity extends MimicryActivity implements MimicryPreviewContract.View {

    private static final String TAG = "MimicryPreviewActivity";

    //labels on the side, displaying the current target emotion to mimicry
    @BindView(R.id.mimikry_emotion_happy)
    TextView happyLabel;
    @BindView(R.id.mimikry_emotion_sad)
    TextView sadLabel;
    @BindView(R.id.mimikry_emotion_surprised)
    TextView surprisedLabel;
    @BindView(R.id.mimikry_emotion_angry)
    TextView angryLabel;

    private  MimicryPreviewContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mimicry_preview);
        ButterKnife.bind(this);
        presenter = new MimicryPreviewPresenter();
        setTimer();
    }

    @Override
    public String getTask() {
        return "PREVIEW";
    }

    @Override
    public void setEmotionLabel(String emotion) {
        Drawable emotionToMimicryLabel = ContextCompat.getDrawable(this, R.drawable.label);
        emotionToMimicryLabel.setColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.claret, null), PorterDuff.Mode.SRC));

        switch (emotion) {
            case HAPPY:
                happyLabel.setBackground(emotionToMimicryLabel);
                break;
            case SURPRISED:
                surprisedLabel.setBackground(emotionToMimicryLabel);
                break;
            case SAD:
                sadLabel.setBackground(emotionToMimicryLabel);
                break;
            case ANGRY:
                angryLabel.setBackground(emotionToMimicryLabel);
                break;
            default:
                Log.d(TAG, "someting went wrong with passing the emotion:" + emotion);
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.bind(this);
        presenter.updateEmotionLabel();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.unbind();
    }
}

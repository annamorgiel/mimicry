package de.emotisk.scott.playingfields.mimicry;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import de.emotisk.scott.R;
import de.emotisk.scott.playingfields.mimicry.utils.Prefs;
import de.fraunhofer.iis.sentinelruntime.LicenseHelper;
import iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment;

import static com.google.common.primitives.Ints.checkedCast;

/**
 * Created by Anna Morgiel on 24.04.2018.
 * Parent Activity for specialised MimicryActivity Preview and MimicryNoPreviewActivity
 * we choose an emotion to mimicry from the emotions String[]
 * set a label with chosen emotion during the mimicry quest and start the timer that moves us
 * to the feedback activity after the timeout of taskDurationInSeconds
 */
public abstract class MimicryActivity extends AppCompatActivity implements CallBackScoreListener, MimicryContract.View {

    private static final String TAG = "MimicryActivity.class";

    private String productKey;
    private String emotion = null;
    private String taskType;

    private int progressUpdateCount;

    private Handler cleanProgressBarHandler;

    @BindView(R.id.mimicry_timer)
    ProgressBar progressBarTimer;

    @BindView(R.id.mimicry_emotion_label_task_progress)
    RoundCornerProgressBar progressBarScore;

    @BindView(R.id.score_display)
    TextView scoreDisplay;

    private MimicryContract.Presenter presenter;
    public static final int TASK_DURATION_IN_SECONDS = 15;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new MimicryPresenter(new Prefs(this), AppDatabase.getDatabase(this));
        presenter.bind(this);
        productKey = getString(R.string.productKeyMimicry);
        presenter.clearData();

        presenter.setGameDetails();

        presenter.fetchEmotionToMimicry();
        presenter.fetchTaskType();
        //use the main looper on the thread where progress bar gets updated to
        //set score = 0 if the new results are not there after 1 second
        cleanProgressBarHandler = new Handler(Looper.getMainLooper());
        if (savedInstanceState == null) {
            initFaceRecognition();
        }
        presenter.addGameDetails(emotion, taskType);
    }

    public void setTaskType(String mimicryType) {
        this.taskType = mimicryType;
    }

    public String getEmotion() {
        return emotion;
    }

    private void initFaceRecognition() {
        getFragmentManager().beginTransaction().replace(R.id.mimicry_shore_container,
                CameraPreviewFragment.newInstance()).commit();
    }

    /**
     * User imitates the given emotion for chosen count of taskDurationInSeconds.
     * The time left gets displayed by the round progress bar
     **/
    protected void setTimer() {
        new CountDownTimer(TASK_DURATION_IN_SECONDS * 1000, 1000) {
            public void onTick(long millisUntilFinished) {
                int progressTime = checkedCast(millisUntilFinished / 1000);
                progressBarTimer.setProgress(progressTime);
            }

            public void onFinish() {
                //TODO add postHandler: Update: not necessary?
                HandlerThread thread = new HandlerThread("MyHandlerThread");
                thread.start();
                Handler handler = new Handler(thread.getLooper());

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        presenter.saveGameToDatabase();
                    }
                });

                Intent intent = new Intent(MimicryActivity.this, MimicryFeedback.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        }.start();
    }

    /**
     * check the SHORE license
     **/
    @Override
    protected void onResume() {
        if (LicenseHelper.checkLicense() == false) {
            LicenseHelper licenseHelper = LicenseHelper.getInstance(this, null);
            licenseHelper.setProductKey(productKey);
            //this starts a new activity
            licenseHelper.getLicense();
        }
        super.onResume();
    }

    /**
     * callback from the CameraPreviewFragment
     * presenter gets notified and saves the latest score for the target emotion
     **/
    public void updateScore(MimicryScore score) {
        presenter.updateGameStatus(score, emotion);
    }

    public void updateProgressBar(float currentScore, float maxPercentageScore) {
        progressBarScore.setSecondaryProgress(maxPercentageScore);
        progressBarScore.setProgress(currentScore);
        Log.i(TAG, "progressUpdateCount: " + Integer.toString(progressUpdateCount++));
        String scoreToDisplay = formatScoreToDisplay(currentScore);
        scoreDisplay.setText(scoreToDisplay + " %");
        postProgressBarUpdate();
    }

    protected void postProgressBarUpdate() {
        cleanProgressBarHandler.removeCallbacksAndMessages(null);
        cleanProgressBarHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                progressBarScore.setProgress(0.0f);
                scoreDisplay.setText("0.0");
            }
        }, 1000);
    }

    public abstract String getTask();

    @Override
    public void setEmotion(String emotion) {
        this.emotion = emotion;
    }

    private String formatScoreToDisplay(float currentScore) {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US);
        DecimalFormat format_decimal = new DecimalFormat("##.##", symbols);
        format_decimal.setRoundingMode(RoundingMode.CEILING);
        return format_decimal.format(currentScore);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.bind(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.unbind();
    }

    @Override
    @OnClick(R.id.back)
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }
}

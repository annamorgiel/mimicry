package de.emotisk.scott.playingfields.mimicry;

public class MimicryNoPreviewPresenter implements MimicryNoPreviewContract.Presenter {


    private MimicryNoPreviewContract.View view;

    @Override
    public void bind(MimicryNoPreviewContract.View view) {
        this.view = view;
    }

    @Override
    public void unbind() {
        this.view = null;
    }

    @Override
    public void updateEmotionDetails() {
        if(view == null){
            return;
        }
        String emotion = view.getEmotion();
        view.setEmotionDetails(emotion);

    }

    @Override
    public void updateEmotionLabel() {
        if(view == null){
            return;
        }
        String emotion = view.getEmotion();
        view.setEmotionLabel(emotion);
    }
}

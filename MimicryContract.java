package de.emotisk.scott.playingfields.mimicry;

/**
 * Created by Anna Morgiel on 29.08.2018.
 *  Container for MV from MVP structure
 */
public interface MimicryContract {

    interface Presenter{
        void bind(View view);
        void unbind();

        void clearData();

        void setGameDetails();

        void fetchEmotionToMimicry();
        void fetchTaskType();

        void addGameDetails(String emotion, String taskType);

        void updateGameStatus(MimicryScore score, String emotion);

        void saveGameToDatabase();
    }
    interface View{
        String getTask();

        void setEmotion(String emotion);

        void setTaskType(String mimicryType);

        void updateProgressBar(float currentScore, float maxPercentageScore);
    }
}


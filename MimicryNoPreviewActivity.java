package de.emotisk.scott.playingfields.mimicry;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.emotisk.android.image.ImageViewFromResourceHelper;
import de.emotisk.scott.R;
import de.emotisk.service.type.BaseMediaStimulus;

import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.ANGRY;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.HAPPY;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.SAD;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.SURPRISED;

public class MimicryNoPreviewActivity extends MimicryActivity implements MimicryNoPreviewContract.View{

    private static final String TAG = "MimicryNoPreview";
    @BindView(R.id.mimicry_emotion_label_task)
    TextView emotionTaskLabel;
    @BindView(R.id.mimicry_emotion_label)
    TextView emotionLabel;

    @BindView(R.id.mimicry_emotion_iv)
    ImageView emotionPicture;
    @BindView(R.id.task_layout)
    View taskLayout;
    @BindView(R.id.mimicry_no_preview_layout)
    View mimicryNoPreviewLayout;

    private int CHOSEN_ACTOR_ID = 4;
    private int emotionId = -1;
    private MimicryNoPreviewContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mimicry_no_preview);
        ButterKnife.bind(this);
        presenter = new MimicryNoPreviewPresenter();
        loadStimuliPicture();
        setTaskTimer();
    }

    private String getEmotionNameInGerman(String emotion) {
        String emotionNameInGerman;
        switch (emotion) {
            case HAPPY:
                emotionNameInGerman = getString(R.string.german_happy);
                break;
            case SAD:
                emotionNameInGerman = getString(R.string.german_sad);
                break;
            case SURPRISED:
                emotionNameInGerman = getString(R.string.german_surprised);
                break;
            case ANGRY:
                emotionNameInGerman = getString(R.string.german_angry);
                break;
            default:
                emotionNameInGerman = getString(R.string.error);
                break;
        }
        return emotionNameInGerman;
    }

    @Override
    public String getTask() {
        return getString(R.string.task_type_mimicry_no_preview);
    }

    @Override
    public void setEmotionLabel(String emotion) {
        emotionLabel.setText(getEmotionNameInGerman(emotion));
    }

    protected void setTaskTimer() {
        CountDownTimer presentTask = new CountDownTimer(5000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                hideTaskAndDisplayMimicryNoPreview();
                setTimer();
            }
        }.start();
    }

    private void hideTaskAndDisplayMimicryNoPreview() {
        taskLayout.setVisibility(View.GONE);
        mimicryNoPreviewLayout.setVisibility(View.VISIBLE);
        setEmotionLabel(getEmotion());
    }

    private void loadStimuliPicture() {
        ImageViewFromResourceHelper.fillImage(emotionPicture, MimicryNoPreviewActivity.this,
                "actors/" + BaseMediaStimulus.getStillURLName(CHOSEN_ACTOR_ID, emotionId));
    }

    @Override
    public void setEmotionDetails(String emotion) {
        switch (emotion) {
            case HAPPY:
                emotionId = 19;
                break;
            case SAD:
                emotionId = 28;
                break;
            case SURPRISED:
                emotionId = 29;
                break;
            case ANGRY:
                emotionId = 4;
                break;
            default:
                emotionId = -1;
                break;
        }
        emotionTaskLabel.setAllCaps(true);
        emotionTaskLabel.setText(getEmotionNameInGerman(emotion));
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.bind(this);
        presenter.updateEmotionDetails();
        presenter.updateEmotionLabel();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.unbind();
    }
}

package de.emotisk.scott.playingfields.mimicry;

import android.arch.persistence.room.TypeConverter;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * The Converter class is required for storing in a db with
 * complex types, it preserves many Game
 * Objects in a JSON format, one entry contains
 * Lists of each Emotion that were fetched during the
 * mimicry task, gameID, userTaskPhaseId
 * Both directions for posting and retrieving the data
 * */
public class Converter {

    /**
     * we pass the
     * @param valueList that is a list of scores for one specific
     *                  emotion {angry, happy, sad, surprised} emotion
     * @return a very long string that can be easily saved in a db
     */
    @TypeConverter
    public static String fromFloatsToString(List<Float> valueList) {

        JSONArray array = new JSONArray();
        for (Float value : valueList) {
            array.put(value);
        }

        return array.toString();
    }

    /**
     *
     * @param json String format was easily saved in a db
     * @return a float emotion list of a specific emotion
     */
    @TypeConverter
    public static List<Float> fromStringToFloats(String json) {
        try {
            JSONArray array = new JSONArray(json);
            List<Float> output = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {
                output.add(
                        Float.parseFloat(
                                array.getString(i)));
            }
            return output;
        } catch (JSONException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }
}

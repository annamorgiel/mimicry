package de.emotisk.scott.playingfields.mimicry;

public interface MimicryPreviewContract {

    interface Presenter{
        void bind(MimicryPreviewContract.View view);
        void unbind();

        void updateEmotionLabel();
    }

    interface View{
        void setEmotionLabel(String emotion);
        String getEmotion();
    }
}

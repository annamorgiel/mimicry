package de.emotisk.scott.playingfields.mimicry;


/**
 * (sub)Model Class with current parameters
 * Values from this class get passed to Game
 * which will be preserved to a db
 *
 * update: object for passing data. Prefs is the new model.
 */
public class MimicryScore {

    private float happy;
    private float sad;
    private float angry;
    private float surprised;

    public MimicryScore(float happy,
                        float sad,
                        float angry,
                        float surprised) {
        this.happy = happy;
        this.sad = sad;
        this.angry = angry;
        this.surprised = surprised;
    }

    public float getHappy() {
        return happy;
    }

    public void setHappy(float happy) {
        this.happy = happy;
    }

    public float getSad() {
        return sad;
    }

    public void setSad(float sad) {
        this.sad = sad;
    }

    public float getAngry() {
        return angry;
    }

    public void setAngry(float angry) {
        this.angry = angry;
    }

    public float getSurprised() { return surprised; }

    public void setSurprised(float surprised) {
        this.surprised = surprised;
    }
}

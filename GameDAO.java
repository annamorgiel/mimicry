package de.emotisk.scott.playingfields.mimicry;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import de.emotisk.service.type.User;

/**
 * Created by Anna Morgiel on 13.07.2018.
 * CRUD; Will be used in db.gameDao().getAll()
 */
@Dao
public interface GameDAO {

    @Query("SELECT * FROM game")
    List<Game> getAll();

    //@Query("SELECT * FROM game WHERE uid IN (:gameIds)")
    //List<Game> loadAllByIds(int[] gameIds);

    //@Query("SELECT * FROM game WHERE target LIKE :target")
    //User findByTarget(String target);

    //@Insert(onConflict = OnConflictStrategy.REPLACE)
    //void insertAll(Game... games);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Game game);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Game> games);

    @Delete
    void delete(Game game);

}

package de.emotisk.scott.playingfields.mimicry;

import android.content.Intent;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.emotisk.scott.R;

public class MimicryNoPreviewIntro extends MimicryIntroActivity {

    @BindView(R.id.mimicry_intro_placeholder)
    ImageView stimuliPicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_mimicry_no_preview_intro);
        ButterKnife.bind(this);
        turnStimuliPictureBlackAndWhite();
        launchNextActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MimicryNoPreviewIntro.this, MimicryNoPreviewActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });
    }

    private void turnStimuliPictureBlackAndWhite() {
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);

        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
        stimuliPicture.setColorFilter(filter);
    }
}

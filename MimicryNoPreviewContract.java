package de.emotisk.scott.playingfields.mimicry;

public interface MimicryNoPreviewContract {

    interface Presenter{
        void bind(View view);
        void unbind();

        void updateEmotionDetails();
        void updateEmotionLabel();
    }
    interface View{
        String getEmotion();

        void setEmotionDetails(String emotion);
        void setEmotionLabel(String emotion);
    }
}

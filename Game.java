package de.emotisk.scott.playingfields.mimicry;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anna Morgiel on 13.07.2018.
 * Entity class models the structure of the db
 * here we use variable names for the column names
 * primaryKey get autoincremented
 */
@Entity
public class Game {

    @PrimaryKey(autoGenerate = true)
    private int uid;
    private int mimicryUserId;
    private String target;
    private String taskType;

    //TODO MIMICRY REFACTOR 2:
    //DONE: add userID which extends the db
    //it will be incremented in the MimicryPreviewIntro
    private List<Float> happy = new ArrayList<>();
    private List<Float> sad = new ArrayList<>();
    private List<Float> angry = new ArrayList<>();
    private List<Float> surprised = new ArrayList<>();

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public void addHappy(float value) {
        happy.add(value);
    }

    public void addSad(float value) {
        sad.add(value);
    }

    public void addSurprised(float value) {
        surprised.add(value);
    }

    public void addAngry(float value) {
        angry.add(value);
    }

    public List<Float> getHappy() {
        return happy;
    }

    public void setHappy(List<Float> happy) {
        this.happy = happy;
    }

    public List<Float> getSad() {
        return sad;
    }

    public void setSad(List<Float> sad) {
        this.sad = sad;
    }

    public List<Float> getAngry() {
        return angry;
    }

    public void setAngry(List<Float> angry) {
        this.angry = angry;
    }

    public List<Float> getSurprised() {
        return surprised;
    }

    public void setSurprised(List<Float> surprised) {
        this.surprised = surprised;
    }


    public int getMimicryUserId() {
        return mimicryUserId;
    }

    public void setMimicryUserId(int mimicryUserId) {
        this.mimicryUserId = mimicryUserId;
    }

    @Override
    public String toString() {
        return "Game{" +
                "testPhaseUserId=" + mimicryUserId +
                "uid=" + uid +
                ", happy=" + happy +
                ", sad=" + sad +
                ", angry=" + angry +
                ", surprised=" + surprised +
                ", target='" + target + '\'' +
                ", taskType='" + taskType + '\'' +
                '}';
    }
}


package de.emotisk.scott.playingfields.mimicry;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import butterknife.BindView;
import butterknife.OnClick;
import de.emotisk.scott.R;
import de.emotisk.scott.playingfields.mimicry.utils.Prefs;

import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.ANGRY;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.HAPPY;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.SAD;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.SURPRISED;

/**
 * Created by Anna Morgiel on 28.06.2018.
 */
public abstract class MimicryIntroActivity extends AppCompatActivity {

    @BindView(R.id.mimicry_intro_start_button)
    TextView launchNextActivity;

    private String[] emotions = {HAPPY, SAD, ANGRY, SURPRISED};
    Prefs prefs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        saveEmotionsToMimicry();
    }

    /**
     * for our usability test we want user to test all 4 Emotions in both scenarios
     * we preserve the set with 4 emotions in SharedPreferences as "TestedEmotions"
     */
    private void saveEmotionsToMimicry() {
        prefs = new Prefs(this);
        Set<String> set = new HashSet<String>(Arrays.asList(emotions));
        prefs.setTestedEmotions(set);
    }

    /**
     * if the activity gets reused, we want new set of 4 emotions to be preserved ins SharedPreferences
     */
    @Override
    protected void onRestart() {
        super.onRestart();
        saveEmotionsToMimicry();
    }

    @Override
    @OnClick(R.id.back_button)
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }

}
package de.emotisk.scott.playingfields.mimicry;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.emotisk.android.image.ImageViewFromResourceHelper;
import de.emotisk.scott.R;
import de.emotisk.scott.playingfields.feedback.BackgroundView;
import de.emotisk.scott.playingfields.mimicry.utils.Prefs;
import de.emotisk.service.type.BaseMediaStimulus;

import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.ANGRY;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.HAPPY;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.MAX_ANGRY;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.MAX_HAPPY;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.MAX_SAD;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.MAX_SURPRISED;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.SAD;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.SURPRISED;

public class MimicryFeedback extends AppCompatActivity {

    @BindView(R.id.mimicry_intro_placeholder)
    ImageView emotionPicture;
    @BindView(R.id.mimicry_emotion_label)
    TextView emotionLabel;
    @BindView(R.id.mimicry_score)
    TextView mimicryScore;

    @BindView(R.id.feedbackTextTitle)
    TextView feedbackTextTitle;
    @BindView(R.id.feedbackText)
    TextView feedbackText;

    @BindView(R.id.next)
    TextView next;

    private static final int CHOSEN_ACTOR_ID = 4;
    private static final String TAG = MimicryFeedback.class.getName();

    private Float happyScore;
    private Float sadScore;
    private Float angryScore;
    private Float surprisedScore;

    private String emotionToMimicry;
    private int emotionId;

    private String task;

    private Prefs prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = new Prefs(this);
        setContentView(R.layout.activity_mimicry_feedback);
        ButterKnife.bind(this);
        fetchEmotionToMimicry();
        setEmotionLabel(emotionToMimicry);
        fetchEmotionScoresFromSharedPreferences();
        fetchAndDisplayScore();
        loadStimuliPicture();
        fetchTaskType();
        setUpHandlerForReadingFromDB();

        //useful for the coding phase, gone for the usability test phase
        mimicryScore.setVisibility(View.GONE);

        launchNewActivityDependingOnTheFlowPoint();
    }

    private void launchNewActivityDependingOnTheFlowPoint() {
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                if (playedAllEmotions()) {
                    if (task.equals("NO_PREVIEW")) {
                        intent = new Intent(MimicryFeedback.this, MimicryPreviewIntro.class);
                    } else {
                        intent = new Intent(MimicryFeedback.this, MimicryNoPreviewIntro.class);
                    }
                    prefs.clearSharedPreferences();
                } else {
                    if (task.equals("NO_PREVIEW")) {
                        intent = new Intent(MimicryFeedback.this, MimicryNoPreviewActivity.class);
                    } else {
                        intent = new Intent(MimicryFeedback.this, MimicryPreviewActivity.class);
                    }
                }
                startActivity(intent);
            }
        });
    }

    private void setUpHandlerForReadingFromDB() {
        HandlerThread thread = new HandlerThread("MyHandlerThread");
        thread.start();
        Handler handler = new Handler(thread.getLooper());

        handler.post(new Runnable() {
            @Override
            public void run() {
                AppDatabase db = AppDatabase.getDatabase(MimicryFeedback.this);
            }
        });
    }

    private void fetchAndDisplayScore() {
        Float achievedScore = getScore(emotionToMimicry);
        giveFeedback(achievedScore);
    }

    /**This method was used during the test phase,
     * @return if the user played all 4 emotions in one taskType
     * which means played mimicry with each emotion exactly once
     */
    private boolean playedAllEmotions() {
        Set<String> set = prefs.getTestedEmotions();
        if (set.size() == 0 || set.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    @NonNull
    private void fetchEmotionToMimicry() {
        setEmotionToMimicry(prefs.getEmotionToMimicry());
    }

    private void setEmotionLabel(String emotion) {
        String emotionNameInGerman;
        switch (emotion) {
            case HAPPY:
                emotionNameInGerman = getString(R.string.german_happy);
                break;
            case SAD:
                emotionNameInGerman = getString(R.string.sad_german);
                break;
            case SURPRISED:
                emotionNameInGerman = getString(R.string.surprised_german);
                break;
            case ANGRY:
                emotionNameInGerman = getString(R.string.angry_german);
                break;
            default:
                emotionNameInGerman = getString(R.string.error);
                break;
        }
        emotionLabel.setText(emotionNameInGerman);
    }

    private void setEmotionToMimicry(String emotionToMimicry) {
        this.emotionToMimicry = emotionToMimicry;
    }

    private void fetchEmotionScoresFromSharedPreferences() {
        happyScore = prefs.getPercentageScoreForEmotion(MAX_HAPPY);
        sadScore = prefs.getPercentageScoreForEmotion(MAX_SAD);
        angryScore = prefs.getPercentageScoreForEmotion(MAX_ANGRY);
        surprisedScore = prefs.getPercentageScoreForEmotion(MAX_SURPRISED);
    }

    private float getScore(String emotion) {
        switch (emotion) {
            case HAPPY:
                emotionId = 19;
                return happyScore;
            case SAD:
                emotionId = 28;
                return sadScore;
            case SURPRISED:
                emotionId = 29;
                return surprisedScore;
            case ANGRY:
                emotionId = 4;
                return angryScore;
            default:
                break;
        }
        return 0;
    }

    private void giveFeedback(Float achievedScore) {
        /*not in design, but useful for testing purposes
        String score = String.format("%.2f", achievedScore);
        mimicryScore.append(score + "%");*/
        int successCount = prefs.getSuccessCount();
        if (successCount < 3) {
            BackgroundView view = (BackgroundView) findViewById(R.id.incorrect_background);
            view.setBackground(R.drawable.tile_bg_incorrect_feedback);
            feedbackTextTitle.setText(R.string.mimicry_feedback_negative_1);
            feedbackText.setText(R.string.mimicry_feedback_negative_2);
        }
    }

    private void loadStimuliPicture() {
        ImageViewFromResourceHelper.fillImage(emotionPicture, MimicryFeedback.this,
                "actors/" + BaseMediaStimulus.getStillURLName(CHOSEN_ACTOR_ID, emotionId));
    }

    private void fetchTaskType() {
        setTask(prefs.getTaskTypeForMimicry());
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    @Override
    @OnClick(R.id.back)
    public void onBackPressed() {
        super.onBackPressed();

        this.overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }
}

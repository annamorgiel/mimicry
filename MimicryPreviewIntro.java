package de.emotisk.scott.playingfields.mimicry;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import butterknife.ButterKnife;
import de.emotisk.scott.R;
import de.emotisk.scott.playingfields.mimicry.utils.Prefs;

public class MimicryPreviewIntro extends MimicryIntroActivity {

    private Prefs prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = new Prefs(this);
        this.setContentView(R.layout.activity_mimicry_preview_intro);
        ButterKnife.bind(this);
        setCurrentMimicryUserId();

        //TODO MIMICRY: turn task black and white like in other intro? if so, then move to mimicry!

        launchNextActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MimicryPreviewIntro.this, MimicryPreviewActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });
    }

    private void setCurrentMimicryUserId() {
        //DONE: ?: REFACTOR MIMICRY: if( userID == null), Add initial userID, save it  to SharedPref, increment in on resume
        int previousMimicryUserId = prefs.getUserTestPhaseId();
        prefs.saveUserTestPhaseId(previousMimicryUserId);
    }

    @Override
    protected void onStart() {
        super.onStart();
        setCurrentMimicryUserId();
    }
}

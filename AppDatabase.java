package de.emotisk.scott.playingfields.mimicry;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;

/**
 * Created by Anna Morgiel on 13.07.2018.
 * gameDAO Data Access Object for interacting with a db
 * and and instance of a db implemented with a singleton pattern
 */

@Database(entities = {Game.class}, version = 2)
@TypeConverters({Converter.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract GameDAO gameDao();
    private static AppDatabase INSTANCE;


    //Singleton pattern for Room db
    public static AppDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, "mimicry_database")
                            .addMigrations(AppDatabase.MIGRATION_1_2)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE game "
                    + " ADD COLUMN mimicryUserId INTEGER NOT NULL DEFAULT 0");
        }
    };
}


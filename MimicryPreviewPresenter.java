package de.emotisk.scott.playingfields.mimicry;

public class MimicryPreviewPresenter implements MimicryPreviewContract.Presenter {


    private MimicryPreviewContract.View view;

    @Override
    public void bind(MimicryPreviewContract.View view) {
        this.view = view;
    }

    @Override
    public void unbind() {
        view = null;
    }

    @Override
    public void updateEmotionLabel() {
        if (view == null) {
            return;
        }
        String emotion = view.getEmotion();
        view.setEmotionLabel(emotion);
    }
}

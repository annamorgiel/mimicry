package de.emotisk.scott.playingfields.mimicry;

/**
 * Created by Anna Morgiel on 14.05.2018.
 * When a new score arrives, the CallBackScoreListener gets notified.
 * It is responsible for updating the score progress bar and preserving the score
 * in the Room db
 */
public interface CallBackScoreListener {

    void updateScore(MimicryScore score);

}

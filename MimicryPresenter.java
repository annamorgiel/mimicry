package de.emotisk.scott.playingfields.mimicry;

import java.util.Random;
import java.util.Set;

import de.emotisk.scott.playingfields.mimicry.MimicryContract.View;
import de.emotisk.scott.playingfields.mimicry.utils.Prefs;

import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.ANGRY;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.HAPPY;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.SAD;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.SURPRISED;

/** Activity only contains a reference to the presenter and all interactions will be handled here*/
public class MimicryPresenter implements MimicryContract.Presenter{

    private View view;
    private Prefs prefs;
    private AppDatabase database;
    private Game game;

    //DONE: pass Prefs (model) in constructor params. Presenter talks, view doesn't. Prefs get created in activity.
    public MimicryPresenter(Prefs prefs, AppDatabase database) {
        this.prefs = prefs;
        this.database = database;
    }

    //we bind MimicryConract.View in onStart
    @Override
    public void bind(View view) {
        this.view = view;
    }

    //unbind in onStop
    @Override
    public void unbind() {
        view = null;
    }

    /** Each player from the usability study will be saved as a separate game*/
    public void clearData(){
        prefs.clearSharedPreferences();
    }

    //check for null to avoid crushing
    @Override
    public void setGameDetails() {
        if(view == null) {
            return;
        }
        String emotion = selectNextEmotionToMimicry();
        String task = view.getTask();

        prefs.saveGameDetails(emotion, task);
    }

    private String selectNextEmotionToMimicry() {
        //TODO this would crash if the set was null
        Set<String> set = prefs.getTestedEmotions();
        int rnd = 0;
        if (set != null) {
            rnd = new Random().nextInt(set.size());
        }
        String emotionToMimicryFromStringArray = String.valueOf(set.toArray()[rnd]);
        set.remove(emotionToMimicryFromStringArray);
        prefs.setTestedEmotions(set);
        return emotionToMimicryFromStringArray;
    }

    /** Presenter commands view to set layout details and for saving in a db*/
    public void fetchEmotionToMimicry() {
        if (view == null){
            return;
        }
        view.setEmotion(prefs.getEmotionToMimicry());
    }

    @Override
    public void fetchTaskType() {
        if (view == null){
            return;
        }
        view.setTaskType(prefs.getTaskTypeForMimicry());
    }


    public void addGameDetails(String emotion, String taskType) {
        game = new Game();
        game.setTaskType(taskType);
        game.setTarget(emotion);
        int currentId = prefs.getUserTestPhaseId();
        game.setMimicryUserId(currentId);
    }

    @Override
    public void updateGameStatus(MimicryScore score, String emotion) {
        float currentScore = fetchEmotionToMimicryScore(score, emotion);
        float maxPercentageScore = prefs.getPercentageScoreForEmotion(emotion);
        if(view != null) {
            view.updateProgressBar(currentScore, maxPercentageScore);
        }
        markTaskAsSolvedIfTresholdWasReached(currentScore);
        addEmotionScoresToCurrentGame(score);
    }

    @Override
    public void saveGameToDatabase() {
        database.gameDao().insert(game);
    }

    private float fetchEmotionToMimicryScore(MimicryScore score, String emotion) {
        float percentageScore = 0.0f;
        switch (emotion) {
            case HAPPY:
                percentageScore = score.getHappy();
                break;
            case SAD:
                percentageScore = score.getSad();

                break;
            case ANGRY:
                percentageScore = score.getAngry();
                break;
            case SURPRISED:
                percentageScore = score.getSurprised();
                break;
            default:
                //Log.d("", "someting went wrong with passing the emotion:" + emotion);
                break;
        }

        return percentageScore;
    }

    private void markTaskAsSolvedIfTresholdWasReached(float emotionToMiMimicryCurrentScore) {
        float thresholdForSolvingTask = 0.75f;
        if (emotionToMiMimicryCurrentScore > thresholdForSolvingTask) {
            prefs.incrementSuccessCount();
        }
    }

    private void addEmotionScoresToCurrentGame(MimicryScore score) {
        game.addHappy(score.getHappy());
        game.addSad(score.getSad());
        game.addAngry(score.getAngry());
        game.addSurprised(score.getSurprised());
    }
}

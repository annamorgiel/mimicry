//code description below

#Mimicry Module

Mimicry Module is an IT-based training of expressing emotions to improve social and emotional competencies for people who show deficits in those areas. 
I implemented it for the purpose of my bachelor thesis https://www.overleaf.com/read/dwpmsythbmvw.

Mimicry Module is one of many modules of the E.V.A. (Emotionen Verstehen und Ausdrücken) app that has been developed at the University of Potsdam.  
The other Modules are under the NDA. I also evaluated the User Experience and Usability. 
During the study, the data have been collected and may be provided anonymized on the demand for scientific purposes. 
This also applies to the results of UX and Usability study. 
The effectiveness of the training itself has not been evaluated.
The module works as a part of E.V.A. and requires an api_key for the face recognition software.

The user's task is to mimicry the target emotion. Software captivates the facial expressions, 
assesses the adequacy of the performed emotion using an external face recognition software, 
and gives direct feedback to the user.

Users adjust to the dynamically presented feedback. Two variants were implemented, one giving the preview of the user's face and one with no preview.

#Code description

The project has been refactored to the MVP architecture.

MimicryActivity is an abstract parent class to two variants of the game - MimicryPreviewActivity and MimicryNoPreviewActivity.

They set different layouts for the training - one where the user gets to see a live recording of his face and the other where this preview won't be shown.

Both MimicryPreviewActivity and MimicryNoPreviewActivity have intro Screens where the gameflow will be presented: MimicryPreviewIntro and MimicryNoPreviewIntro.

The intro screens will be presented only once but each game type will be played four times to imitate the four basic emotions: happy, surprised, angry, sad.

MimicryFeedback will be displayed after playing each emotion.

MimicryScore is for passing the emotion scores as floats from external CameraPreviewFragment who handles the image processing and face recognition part.

This will be handled by the custom CallBackScoreListener who passes the results from CameraPreviewFragment to the MimicryActivity.

GameDAO, Game, Converter and AppDatabase have been created for saving results from the conducted User Experience (UX) and Usability study.

The basic concepts from both psychology and programming were introduced in the second chapter and in the Appendix,

This work is interdisciplinary, offering possible further continuations as a bachelor or a master thesis.
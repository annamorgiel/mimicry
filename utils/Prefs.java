package de.emotisk.scott.playingfields.mimicry.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.Set;

import static android.content.SharedPreferences.*;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.ANGRY;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.HAPPY;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.MAX_ANGRY;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.MAX_HAPPY;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.MAX_SAD;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.MAX_SURPRISED;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.SAD;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.SUCCESS_COUNT;
import static iis.fraunhofer.de.shoresampleapp.CameraPreviewFragment.SURPRISED;
/** Model */

public class Prefs {
    private static final String USER_TEST_PHASE_ID = "userTestPhaseId";
    private static final String MIMICRY = "Mimicry";
    private static final String EMOTION = "Emotion";
    private static final String TASK_TYPE = "Task Type";
    private static final String DEFAULT = "DEFAULT";
    private static final String TESTED_EMOTIONS = "TestedEmotions";
    private Context context;
    private SharedPreferences prefs;
    private Editor editor;

    public Prefs(Context context) {
        this.context = context;
        setUpSharedPreferences();
    }

    public int getUserTestPhaseId() {
        return prefs.getInt(Prefs.USER_TEST_PHASE_ID, 0);
    }

    @SuppressLint("CommitPrefEdits")
    private void setUpSharedPreferences() {
        prefs = context.getSharedPreferences(Prefs.MIMICRY, Context.MODE_PRIVATE);
        editor = prefs.edit();
    }

    public void saveUserTestPhaseId(int previousUserId) {
        editor
                .putInt(Prefs.USER_TEST_PHASE_ID, previousUserId + 1)
                .commit();
    }

    public String getEmotionToMimicry() {
        return prefs.getString(EMOTION, DEFAULT);
    }

    public String getTaskTypeForMimicry() {
        return prefs.getString(TASK_TYPE, DEFAULT);
    }

    public float getPercentageScoreForEmotion(String emotion) {
        return prefs.getFloat(emotion, 0.0f);
    }

    public void savePercentageScoreForEmotion(String emotion, float score){
        editor
                .putFloat(emotion, score)
                .commit();
    }

    public void saveGameDetails(String emotion, String task) {
        editor
                .putString(EMOTION, emotion)
                .putString(TASK_TYPE, task)
                .commit();
    }

    public void incrementSuccessCount() {
        int currentSuccessCount = prefs.getInt(SUCCESS_COUNT, 0);
        editor
                .putInt(SUCCESS_COUNT, currentSuccessCount + 1)
                .commit();
    }

    public int getSuccessCount(){
        return prefs.getInt(SUCCESS_COUNT, 0);
    }

    public Set<String> getTestedEmotions() {
        return prefs.getStringSet(TESTED_EMOTIONS, null);
    }

    public void setTestedEmotions(Set<String> testedEmotions) {
        editor
                .putStringSet(Prefs.TESTED_EMOTIONS, testedEmotions)
                .commit();
    }

    /**
     * we clear all values from SharedPreferences apart form TestedEmotions set
     * and currentUserId as we want to preserve this information
     **/
    public void clearSharedPreferences() {
        editor.putString(Prefs.EMOTION, Prefs.DEFAULT);
        editor.putFloat(HAPPY, 0.0f);
        editor.putFloat(SAD, 0.0f);
        editor.putFloat(SURPRISED, 0.0f);
        editor.putFloat(ANGRY, 0.0f);

        editor.putFloat(MAX_HAPPY, 0.0f);
        editor.putFloat(MAX_SAD, 0.0f);
        editor.putFloat(MAX_SURPRISED, 0.0f);
        editor.putFloat(MAX_ANGRY, 0.0f);
        editor.putInt(SUCCESS_COUNT, 0);
        editor.commit();
    }
}